#include <pspkernel.h>
#include <pspdebug.h>
#include <pspdisplay.h>
#include <pspctrl.h>
#include <pspaudiocodec.h>
#include <psputility.h>
#include <pspmpeg.h>
#include <pspmpegbase.h>
#include <pspvideocodec.h>
#include <stdlib.h>
#include <string.h>
#include "pspdmacplus.h"

PSP_MODULE_INFO("CSC", PSP_MODULE_KERNEL, 1, 1);
PSP_MAIN_THREAD_ATTR(0);

#define printf	pspDebugScreenPrintf

#define DECIMAL_TO_Q2_7(x) ((int)(x * (1 << 7)) & 0x3FF)

typedef struct SceDmacPlusCscParams {
	uint16_t ctm[3][3]; /* Q3.7 fixed point */
	uint8_t footrom;
	uint8_t padding;
	uint16_t unk3;
} SceDmacPlusCscParams;

typedef struct SceDmacPlusAvcCsc {
	unsigned int height; /* height / 16 */
	unsigned int width;  /* width / 16 */
	unsigned int mode0;
	unsigned int mode1;
	void *buffer0;
	void *buffer1;
	void *buffer2;
	void *buffer3;
	void *buffer4;
	void *buffer5;
	void *buffer6;
	void *buffer7;
} SceDmacPlusAvcCsc;

int sceDmacplus_driver_0xB269EAC9_csc_params(int colorfmt, int stride, const SceDmacPlusCscParams *cscparams);
int sceDmacplus_driver_0x9C492B9B_csc_vme(void *pRGBbuffer, void *pRGBbuffer2, const SceMpegYCrCbBuffer *pYCrCbBuffer);
int sceDmacplus_driver_0xD126494B_csc_avc(void *pRGBbuffer, void *pRGBbuffer2, const SceDmacPlusAvcCsc *csc_params);
int sceDmacplusMe2ScSync(int wait_mode, int *data);
int sceDmacplusAvcSync(int wait_mode, int *data);

int sceSysregAwRegABusClockEnable();
int sceSysregAwRegBBusClockEnable();
int sceSysregAwEdramBusClockEnable();
int sceSysregAwResetDisable();

int sceMeMemory_driver_0xC4EDA9F4_sceMeCalloc(unsigned int num, unsigned int size);
int sceMeWrapper_driver_0x24317CD0(void);
int sceMeVideo_driver_0xE8CD3C75(int unk, unsigned int *buff);
int sceMeVideo_driver_0x21521BE5(unsigned int params[14]);
int sceMePower_driver_0x984E2608(int val);
int sceMePowerSelectAvcClock(int val);
int sceMePowerControlAvcPower(int val);
int sceMeCore_driver_0x905A7500_get_unkVideo(void);
int sceMeCore_driver_0x635397BB_cmd(int cmd, ...);
int sceMeCore_driver_0xFA398D71_cmd2(int cmd, ...);

extern int sceVideocodecStop(unsigned long *Buffer, int Type);
extern int sceVideocodecDelete(unsigned long *Buffer, int Type);
extern int sceMpegBaseCscAvc(ScePVoid pRGBbuffer, ScePVoid pRGBbuffer2, SceInt32 width, SceMpegYCrCbBuffer *pYCrCbBuffer);
extern int sceMpegbase_0530BE4E(unsigned short colorformat);

extern int sceSysregAvcResetDisable(void);

static int done = 0;

void draw_rectangle(int x, int y, int width, int height, unsigned int color);
int exit_callback(int arg1, int arg2, void *common);
int CallbackThread(SceSize args, void *argp);
int SetupCallbacks(void);

#define WIDTH  64
#define HEIGHT 64
#define STRIDE 64

static unsigned char g_srcbuff[STRIDE * HEIGHT * 4] __attribute__((aligned(0x1000)));
//static unsigned char dstbuff[STRIDE * HEIGHT * 4] __attribute__((aligned(64)));

void videocodec_experiment()
{
	/*unsigned int params[6];
	int unknown2 = 0;
	memset(params, 0, sizeof(params));
	params[3] = unknown2;
	params[4] = 3; // pixelformat?
	params[5] = (unsigned int)dest1; // outYuv420Buffer
	uVar1 = copyYCbCr((int *)mpeg,(int *)params,srcBuffer,-1);*/

	/*sceKernelDcacheWritebackInvalidateRange(params[3],sizes[0]);
	sceKernelDcacheWritebackInvalidateRange(params[4],sizes[0]);
	sceKernelDcacheWritebackInvalidateRange(params[5],sizes[2]);
	sceKernelDcacheWritebackInvalidateRange(params[6],sizes[2]);
	sceKernelDcacheWritebackInvalidateRange(params[7],sizes[1]);
	sceKernelDcacheWritebackInvalidateRange(params[8],sizes[1]);
	sceKernelDcacheWritebackInvalidateRange(params[9],sizes[3]);
	sceKernelDcacheWritebackInvalidateRange(params[10],sizes[3]);
	sceKernelDcacheInvalidateRange(params[11],sizes[4] + 0x3f);
	sceKernelDcacheInvalidateRange(params[12],sizes[5] + 0x3f);
	sceKernelDcacheInvalidateRange(params[13],sizes[5] + 0x3f);
	sceKernelDcacheWritebackInvalidateRange(params,0x38);
	iVar1 = sceMeVideo_driver_0x21521BE5(params);*/
	int ret;
	unsigned int stride = STRIDE;
	unsigned int width = WIDTH;
	unsigned int height = HEIGHT;
	unsigned int fbaddr = ((unsigned int)sceDmacplusLcdcGetBaseAddr() & ~0x40000000);
	unsigned char *srcbuff = (unsigned char *)fbaddr + 960*544*4;
	unsigned int srcbuff_size = stride*height*4;
	unsigned char *dstbuff = srcbuff + srcbuff_size;

	int unknown2 = 1;
	unsigned int params[14];
	memset(params, 0, sizeof(params));
	params[0] = width;
	params[1] = height;
	params[2] = unknown2;
	params[3] = (unsigned int)srcbuff;
	params[4] = (unsigned int)srcbuff;
	params[5] = (unsigned int)srcbuff;
	params[6] = (unsigned int)srcbuff;
	params[7] = (unsigned int)srcbuff;
	params[8] = (unsigned int)srcbuff;
	params[9] = (unsigned int)srcbuff;
	params[10] = (unsigned int)srcbuff;
	unsigned int y_plane_size = params[0] * params[1];
	unsigned int uv_plane_size = y_plane_size >> 2;
	params[11] = (unsigned int)dstbuff;
	params[12] = params[11] + y_plane_size;
	params[13] = params[11] + y_plane_size + uv_plane_size;
	sceKernelDcacheWritebackInvalidateRange(params, sizeof(params));
	ret = sceMeVideo_driver_0x21521BE5(params);
	printf("sceMeVideo_driver_0x21521BE5 returned: 0x%X\n", ret);
}

void experiment()
{
	int ret;
	SceMpegYCrCbBuffer cscBuffer __attribute__((aligned(64)));
	memset(&cscBuffer, 0, sizeof(cscBuffer));

	unsigned int stride = STRIDE;
	unsigned int width = WIDTH;
	unsigned int height = HEIGHT;
	unsigned int colorfmt = SCE_DMACPLUS_LCDC_FORMAT_RGBA8888;

	void *fbaddr;
	int fbwidth, fbpixelformat;
	sceDisplayGetFrameBuf(&fbaddr, &fbwidth, &fbpixelformat, PSP_DISPLAY_SETBUF_IMMEDIATE);
	fbaddr = (void *)((unsigned int)sceDmacplusLcdcGetBaseAddr() | 0x40000000);

	unsigned char *srcbuff = (unsigned char *)(((unsigned int)fbaddr + 0) | 0);
	unsigned int srcbuff_size = stride*height*4;
	unsigned char *dstbuff = fbaddr + srcbuff_size;
	unsigned int dstbuff_size = stride*height*4;

	// YUV 4:2:0: for a 2×2 square of pixels, 4 Y samples, 1 U sample and 1 V sample
	unsigned int y_plane_half = (width * height) / 2;
	unsigned int u_v_plane_half = (width * height) / 8;

	unsigned int dst_buff_half_size = stride * height;
	if (colorfmt == SCE_DMACPLUS_LCDC_FORMAT_RGBA8888) {
		dst_buff_half_size *= 2;
	}
	cscBuffer.iFrameBufferWidth16 = width / 16;
	cscBuffer.iFrameBufferHeight16 = height / 16;
	cscBuffer.iUnknown   = 0;
	cscBuffer.iUnknown2  = 1;
	cscBuffer.pYBuffer   = srcbuff;
	cscBuffer.pYBuffer2  = (void *)((char *)cscBuffer.pYBuffer   + y_plane_half);
	cscBuffer.pCrBuffer  = (void *)((char *)cscBuffer.pYBuffer2  + y_plane_half);
	cscBuffer.pCrBuffer2 = (void *)((char *)cscBuffer.pCrBuffer  + u_v_plane_half);
	cscBuffer.pCbBuffer  = (void *)((char *)cscBuffer.pCrBuffer2 + u_v_plane_half);
	cscBuffer.pCbBuffer2 = (void *)((char *)cscBuffer.pCbBuffer  + u_v_plane_half);
	cscBuffer.iFrameHeight = height;
	cscBuffer.iFrameWidth = width;
	cscBuffer.iFrameBufferWidth = stride;

	void *yptr = cscBuffer.pYBuffer;
	void *crptr = cscBuffer.pCrBuffer;
	void *cbptr = cscBuffer.pCbBuffer;
	unsigned char *dest1 = dstbuff;
	unsigned char *dest2 = dstbuff + dst_buff_half_size;
	unsigned int *dest1_u32 = (unsigned int *)dest1;
	unsigned int *dest2_u32 = (unsigned int *)dest2;

	// Clear buffers
	//memset(srcbuff, 0xAA, srcbuff_size);
	memset(dstbuff, 0, dstbuff_size);

	// Change some input values
	//*(unsigned int *)yptr = 0x000000FF;
	//*(unsigned int *)crptr = 0x000000FF;
	//*(unsigned int *)cbptr = 0xFF;

	//sceKernelDcacheWritebackInvalidateRange(srcbuff, srcbuff_size);
	sceKernelDcacheWritebackInvalidateRange(dstbuff, dstbuff_size /* dst_buff_half_size * 2 */);

	printf("Starting CSC...\n");

	//ret = sceMeWrapper_driver_0x24317CD0(); // init
	//printf("sceMeWrapper_driver_0x24317CD0 returned: 0x%X\n", ret);

#if 0
	struct VideoCodecCtx {
		unsigned int params_0;
		unsigned int params_1;
		unsigned int params_2;
		unsigned int params_3;
		void *sc_context; // params[4]
		unsigned int me_context_addr; // params[5]
		unsigned int me_context_size; // params[6]
		unsigned int params_7;
		unsigned int params_8;
	};

	unsigned int params_3;
	unsigned char sc_context[0x100] __attribute__((aligned(64))); // params[4]
	unsigned int me_context_addr; // params[5]
	unsigned int me_context_size; // params[6]
	unsigned int params_7;
	unsigned int params_8;
	unsigned int params_0x17;

	memset(sc_context, 0, sizeof(sc_context));

	{ // sceVideocodecOpen -> sceMeVideo_driver_0xC441994C_cmd2
		ret = sceMeCore_driver_0xFA398D71_cmd2(36);
		me_context_size = ret;
		printf("sceMeCore_driver_0xFA398D71_cmd2(36) returned: 0x%X\n", ret);

		ret = sceMeCore_driver_0xFA398D71_cmd2(35, 512, 512, 512 * 512);
		params_8 = ret;
		printf("sceMeCore_driver_0xFA398D71_cmd2(35) returned: 0x%X\n", ret);
	}

	{ // sceVideocodecGetEDRAM
		ret = sceMeMemory_driver_0xC4EDA9F4_sceMeCalloc((me_context_size + 0x3f) | 0x3f, 1);
		params_0x17 = ret;
		me_context_addr = ret & ~0x3f;
		printf("sceMeMemory_driver_0xC4EDA9F4_sceMeCalloc returned: 0x%X\n", ret);
	}

	params_7 = 16384;

	{ // sceVideocodecInit(opt)
		const int opt = 1;
		int sc_ctx_size = 0x28;
		if (opt != 0) {
			sc_ctx_size = 0x100;
		}
		sceKernelDcacheWritebackInvalidateRange(sc_context, sc_ctx_size);
		if ((me_context_addr & 0x1fffffffU) >= 0x400000) { // 04000000 - VRAM (8MB)
			sceKernelDcacheWritebackInvalidateRange(me_context_addr, me_context_size);
		}
		sceKernelDcacheWritebackInvalidateRange(params_7, params_8);
		{ // sceMeVideo_driver_0xE8CD3C75(opt)

			if (opt == 1) {
				ret = sceMeCore_driver_0x635397BB_cmd(37, params_3, me_context_addr, me_context_size, 0);
				printf("sceMeCore_driver_0x635397BB_cmd(37) returned: 0x%X\n", ret);
				printf("params_3: 0x%X\n", params_3);
			} else if (opt == 0) {
				ret = sceMeCore_driver_0x635397BB_cmd(1, params_3, me_context_addr, params_7);
				printf("sceMeCore_driver_0x635397BB_cmd(1) returned: 0x%X\n", ret);
				ret = sceMePowerControlAvcPower(1);
				printf("sceMePowerControlAvcPower returned: 0x%X\n", ret);
			}
			ret = sceMePower_driver_0x984E2608(1); // meSysregAwEdramBusClockEnable
			printf("sceMePower_driver_0x984E2608 returned: 0x%X\n", ret);
		}
	}

	printf("Video codec init done!\n");
#endif

	//ret = sceMeCore_driver_0x635397BB_cmd(1, params + 3, params[5], params[7]);
	//printf("sceMeCore_driver_0x635397BB_cmd(1) returned: 0x%X\n", ret);

	ret = sceMePowerControlAvcPower(1); // not needed for VME
	printf("sceMePowerControlAvcPower returned: 0x%X\n", ret);

	ret = sceMePower_driver_0x984E2608(1); // meSysregAwEdramBusClockEnable
	printf("sceMePower_driver_0x984E2608 returned: 0x%X\n", ret);

	sceSysregMeBusClockEnable();
	sceSysregAwRegABusClockEnable();
	sceSysregAwRegBBusClockEnable();
	sceSysregAwEdramBusClockEnable();

	sceSysregMeResetDisable();
	sceSysregVmeResetDisable();
	sceSysregAvcResetDisable();
	sceSysregAwResetDisable();

	//ret = sceMePowerSelectAvcClock(2);
	//printf("sceMePowerSelectAvcClock returned: 0x%X\n", ret);

#if 0
	sceUtilityLoadAvModule(PSP_AV_MODULE_AVCODEC);
	sceUtilityLoadAvModule(PSP_AV_MODULE_MPEGBASE);

	ret = sceMpegBaseCscInit(512);
	printf("sceMpegBaseCscInit returned: 0x%X\n", ret);

	unsigned long codec_buffer[96] __attribute__((aligned(64)));
	memset(codec_buffer, 0, sizeof(codec_buffer));
	codec_buffer[0] = 0x5100601;
	codec_buffer[4] = (unsigned long)((char *)(codec_buffer) + 128);
	codec_buffer[11] = 512;
	codec_buffer[12] = 512;
	codec_buffer[13] = 512*512;
	//codec_buffer[11] = width;
	//codec_buffer[12] = height;
	//codec_buffer[13] = width * height;
	ret = sceVideocodecOpen(codec_buffer, 0x1);
	printf("sceVideocodecOpen returned: 0x%X\n", ret);

	codec_buffer[7] = 16384;

	ret = sceVideocodecGetEDRAM(codec_buffer, 0x1);
	printf("sceVideocodecGetEDRAM returned: 0x%X\n", ret);

	//ret = sceMeCore_driver_0x905A7500_get_unkVideo();
	//printf("sceMeCore_driver_0x905A7500_get_unkVideo returned: 0x%X\n", ret);

	ret = sceVideocodecInit(codec_buffer, 0x1);
	printf("sceVideocodecInit returned: 0x%X\n", ret);
	{
		int param_2 = 1;
		int iVar1;
		int size = 0x28;
		if (param_2 != 0) {
			size = 0x100;
		}
		sceKernelDcacheWritebackInvalidateRange(codec_buffer[4], size);
		if ((codec_buffer[5] & 0x1fffffffU) < 0x400000) {
			iVar1 = codec_buffer[7];
		} else {
			sceKernelDcacheWritebackInvalidateRange(codec_buffer[5], codec_buffer[6]);
			iVar1 = codec_buffer[7];
		}
		//sceKernelDcacheWritebackInvalidateRange(iVar1, codec_buffer[8]);
		ret = sceMeCore_driver_0x905A7500_get_unkVideo();
		printf("sceMeCore_driver_0x905A7500_get_unkVideo returned: 0x%X\n", ret);
		ret = sceMeVideo_driver_0xE8CD3C75(param_2, (unsigned int *)codec_buffer);
		printf("sceMeVideo_driver_0xE8CD3C75 returned: 0x%X, buff[2] = 0x%X\n", ret, codec_buffer[2]);
		ret = sceMeCore_driver_0x905A7500_get_unkVideo();
		printf("sceMeCore_driver_0x905A7500_get_unkVideo returned: 0x%X\n", ret);
	}
#endif

#if 0
	static const SceDmacPlusCscParams csc_YCbCr_to_RGB_SDTV = {
		.ctm = {
			{0x95,  0x0,   0xCC},
			{0x95, 0x3CE, 0x398},
			{0x95, 0x102,   0x0}
		},
		.footrom = 0x10,
		.padding = 0x0,
		.unk3 = 0x1
	};

	ret = sceDmacplus_driver_0xB269EAC9_csc_params(colorfmt, stride, &csc_YCbCr_to_RGB_SDTV);
	printf("sceDmacplus_driver_0xB269EAC9_csc_params returned: 0x%X\n", ret);

	ret = sceDmacplus_driver_0x9C492B9B_csc_vme(dest1, NULL, &cscBuffer);
	printf("sceDmacplus_driver_0x9C492B9B_csc_vme returned: 0x%X\n", ret);

	ret = sceDmacplusAvcSync(0, NULL);
	printf("sceDmacplusAvcSync returned: 0x%X\n", ret);
#endif

#if 0
	//srcbuff = 0x04000000;
	int unknown2 = 0;
	unsigned int params[14];
	memset(params, 0, sizeof(params));
	params[0] = width / 16;
	params[1] = height / 16;
	params[2] = unknown2;
	params[3] = (unsigned int)srcbuff;
	params[4] = (unsigned int)srcbuff;
	params[5] = (unsigned int)srcbuff;
	params[6] = (unsigned int)srcbuff;
	params[7] = (unsigned int)srcbuff;
	params[8] = (unsigned int)srcbuff;
	params[9] = (unsigned int)srcbuff;
	params[10] = (unsigned int)srcbuff;
	unsigned int y_plane_size = params[0] * params[1];
	unsigned int uv_plane_size = y_plane_size >> 2;
	params[11] = (unsigned int)dstbuff;
	params[12] = params[11] + y_plane_size;
	params[13] = params[11] + y_plane_size + uv_plane_size;
	sceKernelDcacheWritebackInvalidateRange(params, sizeof(params));
	ret = sceMeVideo_driver_0x21521BE5(params);
	printf("sceMeVideo_driver_0x21521BE5 returned: 0x%X\n", ret);
#endif

	printf("-------------------------\n");
	//printf("yptr[0] = 0x%08X\n", *(unsigned int *)yptr);
	//printf("crptr[0] = 0x%08X\n", *(unsigned int *)crptr);
	//printf("cbptr[0] = 0x%08X\n", *(unsigned int *)cbptr);
	//printf("-------------------------\n");
	//printf("dest1: %08X %08X %08X %08X %08X %08X\n", dest1_u32[0], dest1_u32[1], dest1_u32[2], dest1_u32[3], dest1_u32[4], dest1_u32[5]);
	//printf("dest2: %08X %08X %08X %08X %08X %08X\n", dest2_u32[0], dest2_u32[1], dest2_u32[2], dest2_u32[3], dest2_u32[4], dest2_u32[5]);

	for (unsigned int i = 0; i < sizeof(dstbuff); i++) {
		if (dstbuff[i] != 0) {
			//printf("destbuff[%d]: 0x%02X", i, dstbuff[i]);
		}
	}

	//srcbuff = 0x04000000 | 0x40000000;
	srcbuff = (unsigned char *)0x8800000;
	//srcbuff = (void *)((unsigned int)g_srcbuff & ~0x80000000);
	while (1) {
		static int i = 0;
		static int j = 0;
		i++;
		if ((i % 2000) == 0)
			j++;

		memset(srcbuff, 0xFF, srcbuff_size);
		sceKernelDcacheWritebackRange(srcbuff, srcbuff_size);

		//memset(dstbuff, 0, dstbuff_size);
		//sceKernelDcacheWritebackRange(dstbuff, dstbuff_size);

		SceDmacPlusAvcCsc csc;
		csc.height = (height + 15) >> 4;
		csc.width = (width + 15) >> 4;
		csc.mode0 = 0;
		csc.mode1 = 0;
		csc.buffer0 = srcbuff;
		csc.buffer1 = srcbuff;
		csc.buffer2 = srcbuff;
		csc.buffer3 = srcbuff;
		csc.buffer4 = srcbuff;
		csc.buffer5 = srcbuff;
		csc.buffer6 = srcbuff;
		csc.buffer7 = srcbuff;

		static int works = 0;
		static int worked = 0;

		SceDmacPlusCscParams rgb_to_ycbcr_sdtv = {
			.ctm = {
				{DECIMAL_TO_Q2_7(0.257),  DECIMAL_TO_Q2_7(0.504),  DECIMAL_TO_Q2_7(0.098)},
				{DECIMAL_TO_Q2_7(-0.148), DECIMAL_TO_Q2_7(-0.291), DECIMAL_TO_Q2_7(0.439)},
				{DECIMAL_TO_Q2_7(0.439),  DECIMAL_TO_Q2_7(-0.368), DECIMAL_TO_Q2_7(-0.071)},
			},
			.footrom = 16,
			.padding = 0,
			.unk3 = 0x1
		};

		SceDmacPlusCscParams rgb_to_ycbcr_full_range = {
			.ctm = {
				{DECIMAL_TO_Q2_7(0.299),  DECIMAL_TO_Q2_7(0.587),  DECIMAL_TO_Q2_7(0.114)},
				{DECIMAL_TO_Q2_7(-0.169), DECIMAL_TO_Q2_7(-0.331), DECIMAL_TO_Q2_7(0.500)},
				{DECIMAL_TO_Q2_7(0.500),  DECIMAL_TO_Q2_7(-0.419), DECIMAL_TO_Q2_7(-0.081)},
			},
			.footrom = 0,
			.padding = 0,
			.unk3 = 0
		};

		ret = sceDmacplus_driver_0xB269EAC9_csc_params(colorfmt, stride, &rgb_to_ycbcr_full_range);
		if (ret < 0) {
			printf("sceDmacplus_driver_0xB269EAC9_csc_params returned: 0x%X\n", ret);
		}

		ret = sceDmacplus_driver_0xD126494B_csc_avc(dest1, dest1, &csc);
		if (ret < 0) {
			printf("sceDmacplus_driver_0xD126494B_csc_avc returned: 0x%X\n", ret);
		}

		ret = sceDmacplusAvcSync(0, NULL);
		sceKernelDcacheInvalidateRange(dstbuff, dstbuff_size);
		if (ret >= 0) {
			if (!works) {
				printf("Starts working @ %08X -> dest[0] = %08X\n", (unsigned int)srcbuff, dest1_u32[0]);
				works = 1;
			}
		} else { /* Error */
			if (works) {
				printf("Stops  working @ %08X\n", (unsigned int)srcbuff);
				works = 0;
			}
		}

		if (works /*&& dest1_u32[0] != 0x00009400 && dest1_u32[0] != 0x00009900*/) {
			printf("dest1: %08X %08X %08X %08X %08X %08X\n", dest1_u32[0], dest1_u32[1], dest1_u32[2], dest1_u32[3], dest1_u32[4], dest1_u32[5]);
			//printf("dest2: %08X %08X %08X %08X %08X %08X\n", dest2_u32[0], dest2_u32[1], dest2_u32[2], dest2_u32[3], dest2_u32[4], dest2_u32[5]);

			//printf("srcbuff @ %08X -> dest[0] = %08X\n", (unsigned int)srcbuff, dest1_u32[0]);
		}

		worked = works;

		//printf("srcbuff @ %08X -> dest[0] = %08X\n", (unsigned int)srcbuff, dest1_u32[0]);

		//draw_rectangle(10-2, 220-2, 32+4, 32+4, 0xFFFFFFFF);
		//draw_rectangle(10, 220, 32, 32, dest1_u32[0]);
		//draw_rectangle(64+10-2, 220-2, 32+4, 32+4, 0xFFFFFFFF);
		//draw_rectangle(64+10, 220, 32, 32, dest1_u32[1]);
		const unsigned int step = 0x40;
		if (srcbuff < (0x0C000000 - step)) {
			//srcbuff += step;
		}
	}
}

int main(int argc, char *argv[])
{
	SceCtrlData pad;

	pspDebugScreenInit();
	SetupCallbacks();

	sceDisplayWaitVblank();

	printf("CSC experiment (kernel)\n");

	//sceUtilityLoadAvModule(PSP_AV_MODULE_AVCODEC);
	//sceUtilityLoadAvModule(PSP_AV_MODULE_MPEGBASE);

	experiment();

	while (!done) {
		sceCtrlReadBufferPositive(&pad, 1);

		if (pad.Buttons & PSP_CTRL_START)
			done = 1;
	}

	printf("Exiting!\n");

	//sceUtilityUnloadAvModule(PSP_AV_MODULE_MPEGBASE);
	//sceUtilityUnloadAvModule(PSP_AV_MODULE_AVCODEC);

	sceKernelExitGame();
	return 0;
}

void draw_rectangle(int x, int y, int width, int height, unsigned int color)
{
	void *fbaddr;
	int fbwidth, fbpixelformat;
	sceDisplayGetFrameBuf(&fbaddr, &fbwidth, &fbpixelformat, PSP_DISPLAY_SETBUF_IMMEDIATE);

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			char *addr = (char *)fbaddr + (fbwidth * (y + i) + (x + j)) * 4;
			*(unsigned int *)addr = color;
		}
	}

}

int exit_callback(int arg1, int arg2, void *common)
{
	done = 1;
	return 0;
}

int CallbackThread(SceSize args, void *argp)
{
	int cbid;

	cbid = sceKernelCreateCallback("Exit Callback", exit_callback, NULL);
	sceKernelRegisterExitCallback(cbid);
	sceKernelSleepThreadCB();

	return 0;
}

int SetupCallbacks(void)
{
	int thid = 0;

	thid = sceKernelCreateThread("update_thread", CallbackThread,
								 0x11, 0xFA0, 0, 0);
	if (thid >= 0) {
		sceKernelStartThread(thid, 0, 0);
	}

	return thid;
}
