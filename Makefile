TARGET = csckernel
OBJS   = main.o stubs/sceDmacplus_driver.o stubs/sceMeWrapper_driver.o stubs/sceMeCore_driver.o stubs/sceSysreg_driver.o

INCDIR   =
CFLAGS   = -G0 -Wall -O2
CXXFLAGS = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS  = $(CFLAGS)
LIBDIR   =
LDFLAGS  =
LIBS     = -lpspvideocodec -lpspmpegbase -lpspkernel

PSP_FW_VERSION = 660
BUILD_PRX = 1
EXTRA_TARGETS = EBOOT.PBP
PSP_EBOOT_TITLE = CSC Experiment (kernel)

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak

PSP_MS=D

copy: EBOOT.PBP
#	@mkdir -p $(PSP_MS)/PSP/GAME/$(TARGET)
#	@cp EBOOT.PBP $(PSP_MS)/PSP/GAME/$(TARGET)
	@cp EBOOT.PBP $(PSP_MS):\PSP\GAME\$(TARGET)
#	@sync
	@echo "Copied!"

copylinux: EBOOT.PBP
	@mkdir -p $(PSP_MS)/PSP/GAME/$(TARGET)
	@cp EBOOT.PBP $(PSP_MS)/PSP/GAME/$(TARGET)
	@sync
	@echo "Copied!"

copyprx: $(TARGET).prx
	@cp $^ $(PSP_MS)/seplugins/
	@sync
	@echo "Copied!"