#include <stdio.h>

/*
SceDmacPlusCscParams rgb_to_ycbcr_sdtv = {
	.ctm = {
		{0x95,  0x0,   0xCC},
		{0x95, 0x3CE, 0x398},
		{0x95, 0x102,   0x0}
	},
	.unk1 = 0x10,
	.unk2 = 0x0,
	.unk3 = 0x1
};
*/

//static inline unsigned short decimal_to_fixed_2_7()

#define DECIMAL_TO_Q2_7(x) ((int)(x * (1 << 7)) & 0x3FF)

#define T(x) printf("%lf -> 0x%X\n", (double)x, DECIMAL_TO_Q2_7(x))

int main()
{
	T(1.16438356164);
	T(0.0);
	T(1.59602678571);
	T(1.16438356164);
	T(-0.39176229009);
	T(-0.81296764723);
	T(1.16438356164);
	T(2.01723214286);
	T(0.0);
}